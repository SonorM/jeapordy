import { BrowserRouter, Routes , Route } from 'react-router-dom'
import { Host, Game, Home } from './pages/index';

function App() {
  return (
    <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />}/>
          <Route path='host' element={<Host />}/>
          <Route path='game' element={<Game />}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
